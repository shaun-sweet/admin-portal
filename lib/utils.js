const fs = require("fs");

/**
 *
 * @param {string} fileName path to
 */
const createFile = fileName => {
  if (fs.existsSync(fileName)) throw Error("File already exists!");
  const fileOpenId = fs.openSync(fileName, "wx");
  fs.closeSync(fileOpenId);
};

module.exports = {
  createFile
};
