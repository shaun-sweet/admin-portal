/* eslint-disable no-console */
const ConfigParser = require("@webantic/nginx-config-parser");
const { createFile } = require("./utils");

const nginxParser = new ConfigParser();
const { SERVER_ROOT } = process.env;

/**
 *
 * @param {string} filePathLocation file path location string
 */
const readConfigFile = filePathLocation =>
  nginxParser.readConfigFile(filePathLocation);

/**
 *
 * @param {string} domain
 * @param {number} port
 */
function getAppNginxConfig(domain, port) {
  const nginxConfigTemplate = readConfigFile(
    `${__dirname}/../templates/newAppNoSSL.conf`
  );

  nginxConfigTemplate.server.server_name = domain;
  nginxConfigTemplate.server[
    "location /"
  ].proxy_pass = `http://localhost:${port}`;
  return nginxConfigTemplate;
}

/**
 *
 * @param {string} fileName name of file on file system
 * @param {object} nginxConfig nginx json object from readConfigFile return
 * @param {boolean} overwrite
 */
const createConfigFile = (fileName, nginxConfig, overwrite = false) => {
  nginxParser.writeConfigFile(fileName, nginxConfig, overwrite);
};

/**
 *
 * @param {string} domain
 * @param {number} port
 */
const createApplication = (domain, port) => {
  try {
    const fileName = `${SERVER_ROOT}${domain}.conf`;
    const appNginxConfig = getAppNginxConfig(domain, port);

    createFile(fileName);
    createConfigFile(fileName, appNginxConfig, true);
  } catch (error) {
    console.error(error);
    throw error;
  }
};

module.exports = { readConfigFile, createApplication };
